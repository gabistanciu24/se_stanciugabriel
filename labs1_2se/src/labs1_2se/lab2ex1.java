package labs1_2se;
import java.util.Scanner;
//the lab 2 exercises must be inside a package named Lab2
public class lab2ex1 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("type in the first number: ");
		int a=in.nextInt();
		System.out.println("type in the second number: ");
		int b=in.nextInt();
		if(a>b)
			System.out.println("The maximum number between a and b is a: "+a);
		else
			System.out.println("The maximum number between a and b is b: "+b);
	}
}
