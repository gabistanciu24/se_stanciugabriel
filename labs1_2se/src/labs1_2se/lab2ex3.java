package labs1_2se;
import java.util.Scanner;

public class lab2ex3 {
	public static void main(String[] args) {
		Scanner in= new Scanner(System.in);
		
		System.out.println("The lower number of the interval: ");
		int a=in.nextInt();
		
		System.out.println("The higher number of the interval: ");
		int b=in.nextInt();
		int isPrime=0;
		int k=0;
		while(a<=b) {
			isPrime=1;
			for(int i=2;i<=a/2;i++) {
				if(a%i==0) {
					isPrime=0;
					break;
				}
			}
		if(isPrime==1 && a>1) {
			System.out.print(a +" ");
			k++;
		}
		a++;
		}
		System.out.print("\n There are "+k+" prime numbers in the interval");
	}
}
